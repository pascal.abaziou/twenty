class Animal:
    def __init__(self, nom, attributs):
        self.nom = nom
        self.attributs = attributs


    def __repr__(self):
        return str(self.__dict__)

    def persist_repr(self):
        return self.nom + " " + " ".join([str(attr) for attr in self.attributs])

    def has_attribute(self, question):
        # question will be used later
        return self.attributs[question]
