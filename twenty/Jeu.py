from twenty.Animal import Animal
from twenty.Base import Base


def add_question(candidats):
    print("Je ne sais pas faire de différence entre " + ', '.join(map(lambda x: x.nom, candidats)))
    print("Donnez moi une question qui permet de différenciez ces animaux")
    base.questions.insert(0, input())
    for animal in base.animaux:
        if animal in candidats:
            print("Quelle est la réponse à cette question pour " + animal.nom + "?")
            animal.attributs.insert(0, int(input()))
        else:
            animal.attributs.insert(0, -1)
    base.save()


def update_candidat(reponses, animal):
    for i, attribut in enumerate(animal.attributs):
        if attribut == -1:
            animal.attributs[i] = reponses[i]
    base.update_animal(animal)
    base.save()


def new_animal(reponses, candidat):
    print("À quel animal pensiez-vous ?")
    animal = Animal(input(), reponses)
    base.add(animal)
    add_question((animal, candidat))


def jouer():
    candidats = base.animaux
    reponses = [-1 for _ in range(len(base.questions))]
    # for question in base.questions:
    for i, question in enumerate(base.questions):
        print(question)
        reponse = int(input())
        reponses[i] = reponse
        candidats = [c for c in candidats if (c.attributs[i] == reponse or c.attributs[i] == -1)]

        if len(candidats) == 1:
            print("Je pense que c'est {}. Est-ce exact [O]ui/[Non]?".format(candidats[0].nom))
            rep = input()
            if rep.startswith("O"):
                update_candidat(reponses, candidats[0])
            else:
                new_animal(reponses, candidats[0])
            break

        if len(candidats) == 0:
            print("""Je ne connais pas de tel animal
            Pouvez-vous me dire duquel il s'agit ? """)
            base.add(Animal(input(), reponses))
            break

    if len(candidats) > 1:
        add_question(candidats, reponses)


if __name__ == '__main__':
    base = Base()
    while (True):
        jouer()
        print("Voulez-vous jouer ?")
        if input() == 'non':
            break
