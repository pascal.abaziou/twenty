from twenty.Animal import Animal

DATAS_TXT = "resources/datas.txt"

class Base:
    def __init__(self):
        f = open(DATAS_TXT, "r")
        nb_questions = int(f.readline()[:-1])
        nb_animaux = int(f.readline()[:-1])
        self.questions = [f.readline()[:-1] for _ in range(nb_questions)]
        animaux_attr = [f.readline()[:-1] for _ in range(nb_animaux)]
        self.animaux = [Animal(attr[0], [int(i) for i in attr[1:]]) for attr in (a.split() for a in animaux_attr)]
        f.close()


    def add(self, animal):
        self.animaux.append(animal)
        self.save()

    def save(self):
        f = open(DATAS_TXT, "w")
        f.write(str(len(self.questions)) + "\n")
        f.write(str(len(self.animaux)) + "\n")
        f.writelines([q + "\n" for q in self.questions])
        f.writelines([a.persist_repr() + "\n" for a in self.animaux])
        f.close()

    def update_animal(self, animal):
        for i, curr in enumerate(self.animaux):
            if curr.nom == animal.nom:
                self.animaux[i] = animal

