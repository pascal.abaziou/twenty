# twenty

Python implementation of 20 questions game
-> In french 4 devoxxFr

- [x] Jouer
- [x] Enrichir la liste des animaux si pas d'animal correspondant aux réponses
- [x] Enrichir les questions si impossible de choisir entre 2+ animaux (avec ajout valeur -1 sur nouvel attribut sur 
animaux écartés des candidats en amont)
- [x] Remplacer les -1  
- [x] L'IA se trompe car un animal a les mêmes attributs mais n'est pas connu : enrichir les questions et la liste des
animaux
- [ ] Optimiser l'ordre des questions en fonctions des propriétés communes 


conclusion de l 'atelier, on pourrait expliquer que pour les IA avancées :
- ce qu'on a fait là pour faire apprendre à notre IA c'est que fait Google avec ces Captchas
- les IA utilisent des valeurs réelles entre 0 et 1 : le -1 que l'on a mis quand on ne c'est pas c'est 0,5
- utilisent ensuite des algorithmes (analyse d'image, ...) pour valoriser les caractéristiques avec les valeurs 
décimales et pas entières
- comme ce n'est pas 0 ou 1, il y a un taux de certitude
- La différence de notre intelligence humaine : 
on montre à un enfant une photo d'un éléphant et il sait le reconnaître sur d'autres photos, il faut montrer des 
quantités de photos à une IA pour lui faire apprendre.
