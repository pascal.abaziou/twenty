# -- FILE: features/example.feature
# language: fr
Fonctionnalité: Traitement des questions

  Scénario: Un chien est un mammifère
    Etant donné un chien avec l'attribut mammifère
    Quand je pose la question si c'est un mammifère
    Alors la réponse est oui

  Scénario: Une pie n'est pas un mammifère
    Etant donné une pie sans l'attribut mammifère
    Quand je pose la question si c'est un mammifère
    Alors la réponse est non

