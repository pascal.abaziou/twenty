from behave import *

from twenty.Base import Base

use_step_matcher("re")


@step("le jeu")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """


@step("la base est lue")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    context.base = Base()


@step('la première ligne contient "2 3"')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert context.ligne == "2 3", 'premiére ligne %r erronnée' % context.ligne


@step("la liste des questions contient (?P<question>.*)")
def step_impl(context, question):
    """
    :type context: behave.runner.Context
    """
    assert question in context.base.questions

@step("la liste des animaux contient (?P<animal>.*)")
def step_impl(context, animal):
    """
    :type context: behave.runner.Context
    """
    for a in context.base.animaux:
        if a.nom == animal:
            return
    assert False


@step("l'attribut (?P<pos>.*) de (?P<animal>.*) est (?P<value>.*)")  # {pos:d}
def step_impl(context, pos, animal, value):
    """
    :type context: behave.runner.Context
    """
    for a in context.base.animaux:
        if a.nom == animal:
            assert a.attributs[int(pos)] == int(value)
            return
    assert False
