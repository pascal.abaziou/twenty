# -- FILE: features/steps/example_steps.py
from behave import step, use_step_matcher
from twenty.Animal import Animal

use_step_matcher("re")

@step("une? (?P<animal>.*) avec l'attribut mammifère")
def step_impl1(context, animal):
    context.animal = Animal(animal, [True])

@step("une? (?P<animal>.*) sans l'attribut mammifère")
def step_impl1(context, animal):
    context.animal = Animal(animal, [False])

@step("je pose la question si (?P<question>.*)")
def step_impl2(context, question):  # -- NOTE: number is converted into integer
    context.attributeIsTrue = context.animal.has_attribute(0) #r(question)


@step('la réponse est (?P<true_false>.*)')
def step_impl3(context, true_false):
    assert context.attributeIsTrue is (true_false == 'oui')
