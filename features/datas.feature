# -- FILE: features/datas.feature
# language: fr

Fonctionnalité: Accès à la base des questions et animaux
  # Enter feature description here

  Scénario: Lecture du fichier complet et vérification de question
    Etant donné le jeu
    Quand la base est lue
    Alors la liste des questions contient Est-ce un mammifère ?

  Scénario: Lecture du fichier complet et vérification d'un animal
    Etant donné le jeu
    Quand la base est lue
    Alors la liste des animaux contient Chien
    Et l'attribut 0 de Chien est 1
    Et l'attribut 1 de Chien est 1
    Alors la liste des animaux contient Pie
    Et l'attribut 0 de Pie est 0
    Et l'attribut 1 de Pie est 0
    Alors la liste des animaux contient Ours
    Et l'attribut 0 de Ours est 1
    Et l'attribut 1 de Ours est 0


